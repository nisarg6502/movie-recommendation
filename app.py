import streamlit as st
import pickle
import pandas as pd
import requests


def fetch_poster(movie_id):
    url = "https://api.themoviedb.org/3/movie/{}?language=en-US".format(
        movie_id)
    headers = {
        "accept": "application/json",
        "Authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJlZTdhMWJjODg4MWVmOTQxMWU3Zjg3NmU3NmUwZDM3NiIsInN1YiI6IjY0ZGQzYTg3YTNiNWU2MDEzOTAwM2UwYSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.YStsUz5KULjze3ToiHhkCLPtlwY83l3mha3Vg6ZzQ50"
    }
    response = requests.get(url, headers=headers)

    data = response.json()
    return "https://image.tmdb.org/t/p/w500/" + data['poster_path']


def recommend(movie):
    movie_index = movies[movies["title"] == movie].index[0]

    movies_list = sorted(
        list(enumerate(similarity[movie_index])), reverse=True, key=lambda x: x[1])[1:6]
    recommended_movie_names = []
    recommended_movie_posters = []
    for i in movies_list:
        # fetch the movie record corresponding to the index in the distances list
        recommended_movie_names.append(movies.iloc[i[0]].title)
        # fetch the poster corresponding to the movie index
        recommended_movie_posters.append(
            fetch_poster(movies.iloc[i[0]].id))

    return recommended_movie_names, recommended_movie_posters


movies_dict = pickle.load(open('movies_dict.pkl', 'rb'))
movies = pd.DataFrame(movies_dict)

similarity = pickle.load(open('similarity.pkl', 'rb'))

st.title("Movie Recommender System")

selected_movie_name = st.selectbox(
    'Which movie do you like best?', movies["title"].values
)

if st.button("Recommend"):
    names, posters = recommend(selected_movie_name)
    col1, col2, col3, col4, col5 = st.columns(5)
    with col1:
        st.text(names[0])
        st.image(posters[0])
    with col2:
        st.text(names[1])
        st.image(posters[1])
    with col3:
        st.text(names[2])
        st.image(posters[2])
    with col4:
        st.text(names[3])
        st.image(posters[3])
    with col5:
        st.text(names[4])
        st.image(posters[4])
